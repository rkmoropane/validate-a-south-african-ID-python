# Validate a South African ID number 

# How To Run This Project:
This reference repo was based on http://doc.pytest.org/en/latest/goodpractices.html and https://github.com/Umuzi-org/python-pytest-reference-dir-structure#directory-structure. The practice performed in this repo is a recognized best practice and should be followed consistently to make auto-marking automation easier.

## Usage
1. **First, make sure that your virtual environment is created, running and active. If not, type this in your command prompt in vs code:**
```
$ python -m venv <name_of_virtual_environment>
```
Or in Linux:
```
$ python3.9 -m venv <name_of_virtual_environment>
```
2. **How to activate it:**
```
$ ./<name_of_virtual_environment>/Scripts/activate
```
Or in Linux:
```
$ source <name_of_virtual_environment>/bin/activate
```
Done. Now your virtual environment is up and running.

3. **After, install the package in the development mode. This will be installed in your virtualenv.**
```
$ python setup.py develop
```
Or in Linux:
```
$ python3.9 setup.py develop
```
This installs the package. Now the package can be imported from anywhere, so long as your virtualenv is active.

4. **Now run the tests by typing:**
```
pytest 
``` 
Done.
