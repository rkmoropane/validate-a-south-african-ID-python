from datetime import datetime


def validate_sa_id_string_length(id_number):
    total_id_numbers = len(id_number)
    return total_id_numbers == 13


def validate_sa_id_digit_string(id_number):
    return id_number.isdigit()


def date_parse(date_string):
    date_format = "%Y%m%d"
    try:
        birth_date = datetime.strptime(date_string, date_format)
        return True
    except ValueError:
        return False


def validate_sa_id_date(id_number):
    id_birth_date = id_number[0:6]
    birth_year = int(id_birth_date[0:2])
    birth_month = int(id_birth_date[2:4])
    birth_day = int(id_birth_date[4:6])
    current_year = datetime.now().year % 100
    
    if birth_year > current_year:
        birth_year += 1900
    else:
        birth_year += 2000

    birth_date = f"{birth_year}{birth_month}{birth_day}"

    return date_parse(birth_date)


def validate_sa_id_gender_code(gender_code):
    return (gender_code >= 0 and gender_code <= 4999) or (
        gender_code >= 5000 and gender_code <= 9999
    )


def validate_sa_id_citizen_code(citizen_code):
    return citizen_code in [0, 1]


def validate_sa_id_checksum_digit(id_number):
    checksum_digit = int(id_number[12])
    id_numbers = [int(id_number) for id_number in id_number]

    even_id_numbers = id_numbers[0:12:2]
    checksum = sum(even_id_numbers)

    odd_id_numbers = id_numbers[1:12:2]
    odd_id_string = ""
    for number in odd_id_numbers:
        digit = str(number)
        odd_id_string += digit

    odd_id_number = int(odd_id_string)
    doubled_odd_id_string = str(odd_id_number * 2)
    doubled_odd_id_number = [int(id_digit) for id_digit in doubled_odd_id_string]

    checksum += sum(doubled_odd_id_number)
    calculated_checksum_digit = (checksum * 9) % 10

    return calculated_checksum_digit == checksum_digit


def validate_sa_id(id_number):
    id_number = id_number[1:] if id_number.startswith("+") else id_number

    if not (
        validate_sa_id_string_length(id_number)
        and validate_sa_id_digit_string(id_number)
        and validate_sa_id_date(id_number)
        and validate_sa_id_gender_code(int(id_number[6:10]))
        and validate_sa_id_citizen_code(int(id_number[10]))
        and validate_sa_id_checksum_digit(id_number)
    ):
        return False
    else:
        return True


if __name__ == "__main__":
    print(validate_sa_id("-9702285658086"))
    print(validate_sa_id("9602291400087"))
    print(validate_sa_id("6402291400085"))
    print(validate_sa_id("0010255657081"))
