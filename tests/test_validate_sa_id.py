import pytest
from validate_sa_id.validate_sa_id import (
    validate_sa_id,
    validate_sa_id_string_length,
    validate_sa_id_digit_string,
    date_parse,
    validate_sa_id_date,
    validate_sa_id_citizen_code,
    validate_sa_id_gender_code,
    validate_sa_id_checksum_digit,
)


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [("2001014800086", True), ("2909035800085", True)],
)
def test_validate_sa_id(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("03031805808", False)])
def test_validate_sa_id_small_id_string(test_id_number, expected_output):
    output = validate_sa_id_string_length(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output", [("980625485008575", False)]
)
def test_validate_sa_id_long_id_string(test_id_number, expected_output):
    output = validate_sa_id_string_length(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("20010A4800086", False)])
def test_validate_sa_id_id_string_contains_non_digit(test_id_number, expected_output):
    output = validate_sa_id_digit_string(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("+9109155658180", True)])
def test_validate_sa_id_positive_id_string(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_date_string, expected_output",
    [("19761231", True), ("19970219", True)],
)
def test_date_parse_valid_date(test_date_string, expected_output):
    output = date_parse(test_date_string)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_date_string, expected_output",
    [("19761249", False), ("19970431", False)],
)
def test_date_parse_invalid_date(test_date_string, expected_output):
    output = date_parse(test_date_string)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [("7612495748183", False), ("8911311400089", False)],
)
def test_validate_sa_id_date_invalid_date(test_id_number, expected_output):
    output = validate_sa_id_date(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [("6402291400085", True), ("8911301400081", True)],
)
def test_validate_sa_id_date_valid_date(test_id_number, expected_output):
    output = validate_sa_id_date(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_code, expected_output", [(5658, True)])
def test_validate_sa_id_male_gender_code(test_id_code, expected_output):
    output = validate_sa_id_gender_code(test_id_code)
    assert output == expected_output


@pytest.mark.parametrize("test_id_code, expected_output", [(1400, True)])
def test_validate_sa_id_female_gender_code(test_id_code, expected_output):
    output = validate_sa_id_gender_code(test_id_code)
    assert output == expected_output


@pytest.mark.parametrize("test_id_code, expected_output", [(2, False)])
def test_validate_sa_id_invalid_id_citizen_code_number(test_id_code, expected_output):
    output = validate_sa_id_citizen_code(test_id_code)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_code, expected_output",
    [
        (1, True),
        (0, True),
    ],
)
def test_validate_sa_id_valid_id_citizen_code_number(test_id_code, expected_output):
    output = validate_sa_id_citizen_code(test_id_code)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("1012095748185", False)])
def test_validate_sa_id_invalid_id_checksum_number(test_id_number, expected_output):
    output = validate_sa_id_checksum_digit(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("9109155658180", True)])
def test_validate_sa_id_valid_id_checksum_number(test_id_number, expected_output):
    output = validate_sa_id_checksum_digit(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [
        ("9702195573086325", False),
        ("65021955730", False),
    ],
)
def test_validate_sa_id_invalid_id_length_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("7401195573087", True)])
def test_validate_sa_id_valid_id_length_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("380915E564180", False)])
def test_validate_sa_id_nondigit_id_number_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("+0001195573082", True)])
def test_validate_sa_id_positive_id_string_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("-0101195573080", False)])
def test_validate_sa_id_negative_id_string_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("-0102195673086", False)])
def test_validate_sa_id_invalid_year_number_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [("+1812199673085", True), ("2811199673086", True)],
)
def test_validate_sa_id_valid_year_number_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [
        ("8705184860084", True),
        ("6604305748181", True),
    ],
)
def test_validate_sa_id_valid_month_number_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("0316195748181", False)])
def test_validate_sa_id_invalid_month_number_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [
        ("9602291400087", True),
        ("6402291400085", True),
    ],
)
def test_validate_sa_id_valid_day_id_number_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [
        ("7612325748189", False),
        ("9109315658187", False),
        ("9604311400089", False),
        ("9506311400086", False),
        ("8911311400089", False),
        ("7502311400087", False),
    ],
)
def test_validate_sa_id_invalid_day_id_number_main_func(
    test_id_number, expected_output
):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [
        ("9702285658086", True),
        ("9602291400087", True),
    ],
)
def test_validate_sa_id_gender_code_id_number_main_func(
    test_id_number, expected_output
):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize(
    "test_id_number, expected_output",
    [
        ("1109155658188", True),
        ("3704155658085", True),
    ],
)
def test_validate_sa_id_valid_citizen_code_id_number_main_func(
    test_id_number, expected_output
):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("1012095748285", False)])
def test_validate_sa_id_invalid_citizen_code_id_number_main_func(
    test_id_number, expected_output
):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("9109155658180", True)])
def test_validate_sa_id_valid_checksum_id_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output


@pytest.mark.parametrize("test_id_number, expected_output", [("9109157758181", False)])
def test_validate_sa_id_invalid_checksum_id_main_func(test_id_number, expected_output):
    output = validate_sa_id(test_id_number)
    assert output == expected_output
